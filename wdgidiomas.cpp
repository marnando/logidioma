#include "wdgidiomas.h"
#include "ui_wdgidiomas.h"
#include <QStandardItemModel>
#include <QListView>

WdgIdiomas::WdgIdiomas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WdgIdiomas)
{
    ui->setupUi(this);
    criaModelListIdioma();
}

WdgIdiomas::~WdgIdiomas()
{
    delete ui;
}

void WdgIdiomas::criaModelListIdioma()
{
    QStandardItemModel * itemModel = new QStandardItemModel(this);
    QList<QStandardItem *> list;

    list.append(new QStandardItem(QIcon(QPixmap(":/images/ptbr.png")), "Português"));
    list.append(new QStandardItem(QIcon(QPixmap(":/images/en.png")), "English"));
    list.append(new QStandardItem(QIcon(QPixmap(":/images/es.png")), "Español"));

    foreach (QStandardItem * item, list)
        itemModel->appendRow(item);

    ui->m_lvIdiomas->setIconSize(QSize(128, 128));
    ui->m_lvIdiomas->setModel(itemModel);
}

void WdgIdiomas::on_m_lvIdiomas_clicked(const QModelIndex &index)
{
    QString l_locale;
    switch (index.row()) {
    case 1:
        l_locale = "en";
        break;
    case 2:
        l_locale = "es";
        break;
    default:
        l_locale = "pt_BR";
        break;
    }

    emit mudouIdioma((l_locale.compare("pt_BR") != 0) ? l_locale : QString());
    ui->retranslateUi(this);
    on_m_pbCancel_clicked();
}

void WdgIdiomas::on_m_pbCancel_clicked()
{
    close();
    QApplication::setActiveWindow(this);
    setFocus();
}
