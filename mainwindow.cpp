#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "wdgidiomas.h"
#include <QLibraryInfo>
#include <QDebug>
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_wdgIdiomas(new WdgIdiomas(this)),
    m_translator(new QTranslator(this))
{
    ui->setupUi(this);

    ui->m_teTexto->setPlainText(tr("Teste de mudanca de idioma"));
    m_wdgIdiomas->hide();

//    m_listTranslators = QList< QPair<bool, QTranslator*> >();

    connect(m_wdgIdiomas, SIGNAL(mudouIdioma(QString)), SLOT(on_mudarIdioma(QString)));
    connect(ui->m_pb0, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb1, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb2, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb3, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb4, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb5, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb6, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb7, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb8, SIGNAL(clicked()), SLOT(on_senha_clicked()));
    connect(ui->m_pb9, SIGNAL(clicked()), SLOT(on_senha_clicked()));

    on_mudarIdioma();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setIconIdioma(QString a_locale)
{
    int index = 0;
    if (a_locale.compare("en") == 0) {
        index = 1;
    } else
    if (a_locale.compare("es") == 0) {
        index = 2;
    }

    switch (index) {
    case 1:
        ui->m_pbIdiomas->setIcon(QIcon(QPixmap(":/images/en.png")));
        break;

    case 2:
        ui->m_pbIdiomas->setIcon(QIcon(QPixmap(":/images/es.png")));
        break;

    default:
        ui->m_pbIdiomas->setIcon(QIcon(QPixmap(":/images/ptbr.png")));
        break;
    }

    ui->retranslateUi(this);
}

void MainWindow::on_mudarIdioma(QString a_locale)
{
    QString     l_locale;
    QStringList l_locales;
    QStringList l_translationsPath;

    l_locale = QLocale::system().name();

    if (!a_locale.isEmpty()) {
        l_locale = a_locale;
    }

    l_locales += l_locale;
    if (l_locale.contains("_")) {
        l_locale = l_locale.left(l_locale.lastIndexOf("_"));
        l_locales += l_locale;
    }

    l_translationsPath += ":/translations";
    l_translationsPath += "./translations";
    l_translationsPath += QLibraryInfo::location(QLibraryInfo::TranslationsPath);

    QList< QPair<bool, QTranslator*> > l_listAux;

    //Removo qualquer tradução que já exista
//    foreach (l_listAux, m_listTranslators) {
//        int i = 0;
//        if (l_listAux[i].first = true) {
//            l_listAux[i].first = false;
//            QCoreApplication::removeTranslator(l_listAux[i].second);
//        }
//        i++;
//    }

    foreach (QString l_translationPath, l_translationsPath) {
        foreach (QString l_locale, l_locales) {
            qDebug() << qApp->applicationName() + "_" + l_locale;
            if (m_translator->load(qApp->applicationName() + "_" + l_locale, l_translationPath)) {
                QCoreApplication::installTranslator(m_translator);
//                m_listTranslators.append(QPair(true, m_translator));
            }
        }
    }
}

void MainWindow::on_m_pbIdiomas_clicked()
{
    if (m_wdgIdiomas) {
        m_wdgIdiomas->show();
        m_wdgIdiomas->raise();
        QApplication::setActiveWindow(m_wdgIdiomas);
        m_wdgIdiomas->setFocus();

        //Posicionando
#ifndef QT_OS_ANDROID
        m_wdgIdiomas->setMaximumHeight(this->height()*0.60);
        m_wdgIdiomas->setMaximumWidth(this->height()*0.60);
#else
        m_wdgIdiomas->setMinimumHeight(this->height()*0.60);
        m_wdgIdiomas->setMinimumWidth(this->height()*0.60);
#endif
        centralizaTelas(m_wdgIdiomas, 1.2);
    }
}

void MainWindow::on_senha_clicked()
{
    QObject * l_button = sender();
    if (l_button) {
        if (l_button->objectName().compare("m_pb0") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "0");
        } else
        if (l_button->objectName().compare("m_pb1") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "1");
        } else
        if (l_button->objectName().compare("m_pb2") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "2");
        } else
        if (l_button->objectName().compare("m_pb3") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "3");
        } else
        if (l_button->objectName().compare("m_pb4") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "4");
        } else
        if (l_button->objectName().compare("m_pb5") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "5");
        } else
        if (l_button->objectName().compare("m_pb6") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "6");
        } else
        if (l_button->objectName().compare("m_pb7") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "7");
        } else
        if (l_button->objectName().compare("m_pb8") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "8");
        } else
        if (l_button->objectName().compare("m_pb9") == 0) {
            ui->m_teSenha->setText(ui->m_teSenha->toPlainText() + "9");
        }
        ui->m_teSenha->moveCursor(QTextCursor::End);
    }
}

void MainWindow::centralizaTelas(QWidget *a_miMod, double a_spacer)
{
    a_miMod->setWindowFlags(Qt::Dialog);
    a_miMod->setWindowModality(Qt::ApplicationModal);
    QDesktopWidget* l_desk = QApplication::desktop();
    QSize l_newsize = QSize (l_desk->width()/a_spacer,l_desk->height()/a_spacer);

    a_miMod->setVisible(true);

    a_miMod->resize(l_newsize);
    a_miMod->updateGeometry();

    QSize l_size = a_miMod->size();
    int w = l_desk->width();
    int h = l_desk->height();
    int mw = l_size.width();
    int mh = l_size.height();
    int cw = (w/2) - (mw/2);
    int ch = (h/2) - (mh/2);

    a_miMod->move(cw,ch);

    QApplication::setActiveWindow(a_miMod);
    a_miMod->setFocus();
}
