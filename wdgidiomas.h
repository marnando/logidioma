#ifndef WDGIDIOMAS_H
#define WDGIDIOMAS_H

#include <QWidget>

namespace Ui {
class WdgIdiomas;
}

class WdgIdiomas : public QWidget
{
    Q_OBJECT

public:
    explicit WdgIdiomas(QWidget *parent = 0);
    ~WdgIdiomas();

    void criaModelListIdioma();

private slots:
    void on_m_lvIdiomas_clicked(const QModelIndex &index);

    void on_m_pbCancel_clicked();

signals:
    void mudouIdioma(QString a_id);

private:
    Ui::WdgIdiomas *ui;
};

#endif // WDGIDIOMAS_H
