#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>
#include <QMap>

namespace Ui {
class MainWindow;
}

class WdgIdiomas;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
//    QMap<QString, QTranslator*> m_translator;

private:
//    QList< QPair<bool, QTranslator*> > m_listTranslators;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setIconIdioma(QString a_locale);

    void centralizaTelas(QWidget *a_miMod, double a_spacer);

private slots:
    void on_mudarIdioma(QString a_locale = QString());
    void on_m_pbIdiomas_clicked();
    void on_senha_clicked();

private:
    Ui::MainWindow * ui;
    WdgIdiomas * m_wdgIdiomas;
    QTranslator * m_translator;
};

#endif // MAINWINDOW_H
