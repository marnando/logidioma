#-------------------------------------------------
#
# Project created by QtCreator 2014-03-07T13:57:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LogIdioma
TEMPLATE = app

TRANSLATIONS += LogIdioma_en.ts \
        LogIdioma_es.ts

SOURCES += main.cpp\
        mainwindow.cpp \
        wdgidiomas.cpp

HEADERS  += mainwindow.h \
    wdgidiomas.h

FORMS    += mainwindow.ui \
    wdgidiomas.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
        rsc/resources.qrc

